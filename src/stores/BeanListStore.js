import alt from '../alt'
import BeanListActions from '../actions/BeanListActions';


class BeanListStore {
    constructor() {
        this.bindActions(BeanListActions);
        this.loadingBeanList = false;
        this.beanList = [];
    }

    onRequestBeanList() {
        console.log('onRequetsBeanList');
        this.loadingBeanList = true;
    }

    onReceiveBeanList(rawList) {
        console.log('onReceiveBeanList');
        this._init(rawList);
        this.loadingBeanList = false;
    }

    _init(rawList) {
        this.beanList = rawList;
    }
}

export default alt.createStore(BeanListStore);
