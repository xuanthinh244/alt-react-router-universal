import alt from '../alt'
import BeanWebAPI from '../api/BeanWebAPI'

class BeanItemActions {

    receiveBeanItem(beanItem) {
        this.dispatch(beanItem);
    }

    receiveBeanItemError(error) {
        this.dispatch(error);
    }

    requestBeanItem(beanID, callback) {
        var actionDispatcher = this;
        actionDispatcher.dispatch();

        BeanWebAPI.requestBeanItem(beanID).then(function(beanItem) {
            actionDispatcher.actions.receiveBeanItem(beanItem);
            console.log("received bean item", beanItem);
            if (callback)
                callback(null);
        }).catch(function(error) {
            actionDispatcher.actions.receiveBeanItemError(error);
            console.log(error);
            if (callback)
                callback(error);
        });
        console.log("requested bean item");
    }
}

export default alt.createActions(BeanItemActions);
