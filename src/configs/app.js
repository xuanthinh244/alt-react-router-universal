'use strict';

export default {
    googleAnalytics: {
        appId: 'appId-xxxxxxxx'
    },
    addthis: {
        id: 'ra-xxxxxxx'
    },
    baseUrl: 'https://localhost',
    metadata: {
        siteName: 'Demo React JS',
        type: 'website',
        url: 'https://thinhvoxuan.me',
        title: 'Thinh VoXuan',
        description: "Universal application with React",
        facebook: {
            appId: '123'
        }
    }
};
