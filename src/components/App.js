var React = require('react');
import AltContainer from 'alt-container';
import BeanItemStore from '../stores/BeanItemStore'
import BeanListStore from '../stores/BeanListStore'
var { RouteHandler, Link } = require('react-router');
var { PropTypes } = React;

var App = React.createClass({
    render: function() {
        return (
            <div>
                <h1>Beans of War</h1>
                <ul className="navigation">
                    <Link to='/'><li className="navigation-item">HOME</li></Link>
                </ul>
                <AltContainer stores={{
                    BeanListStore: BeanListStore,
                    BeanItemStore: BeanItemStore
                }} >
                    {this.props.children}
                </AltContainer>
            </div>
        );
    }
});

export default App;
