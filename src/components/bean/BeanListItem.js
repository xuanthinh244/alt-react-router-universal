var React = require('react');
var { Link } = require('react-router');

var BeanListItem = React.createClass({

    render() {
        var itemStyle = {
            color: this.props.bean.bean_color
        };
        return(
            <Link to={`/${this.props.bean.bean_id}`}>
                <li style={itemStyle} className="bean-list-item" key={this.props.bean.bean_id}>
                    {this.props.bean.bean_name}
                </li>
            </Link>
        )
    }
});



export default BeanListItem;
