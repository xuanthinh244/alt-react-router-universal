import React from 'react'
import BeanPowerListItem from './BeanPowerListItem'


class BeanProfile extends React.Component  {

    render() {
        var beanColor = this.props.beanItem.bean_color;
        var imgStyle = {
            backgroundColor : beanColor
        };

        var spanStyle = {
            color: beanColor
        };

        if (this.props.beanItem.bean_powers) {
            var beanPowers = this.props.beanItem.bean_powers.map(function(power) {
                return (
                    <BeanPowerListItem power={power} key={power.power_name} />
                )
            });
        }

        return (
            <div>
                <img style={imgStyle} className='beanImage' src='http://lessonpix.com/drawings/5847/90x85/Jelly+Bean.png' />
                <h2>The <span style={spanStyle}>{this.props.beanItem.bean_name}</span> bean</h2>
                <div className='bean-description'>{this.props.beanItem.bean_description}</div>
                <div className='bean-powers'>
                    <h2>Powers</h2>
                    <ul>{beanPowers}</ul>
                </div>
            </div>
        )
    }
};



export default BeanProfile;
