var React = require('react');
import BeanItemStore from '../../stores/BeanItemStore'
import BeanItemActions from '../../actions/BeanItemActions'

import BeanPowerListItem from '../bean/BeanPowerListItem';
import BeanProfile from '../bean/BeanProfile'

class BeanItemPage extends React.Component  {
    componentDidMount() {
        BeanItemActions.requestBeanItem(this.props.params.beanId);
    }
    render() {

        var elementToDisplay = null;
        var currentState = this.props.BeanItemStore;

        // First check if it's loading
        // Then once loaded, check if there's an error
        // Show whatever element is required for each stage

        if (currentState.loadingBeanItem) {
            elementToDisplay = (
                <div className="loading-element">LOADING BEAN...</div>
            )
        } else {
            if (currentState.beanItemError) {
                elementToDisplay = (
                    <div className="error">{currentState.beanItemError}</div>
                )
            } else {
                elementToDisplay = (
                    <BeanProfile beanItem={currentState.beanItem} />
                )
            }
        }

        return (
            <div className="bean-item-page">
                {elementToDisplay}
            </div>
        );
    }

    static fetchData(routerState, callback) {
        var idbeacon = routerState.params.beanId;
        BeanItemActions.requestBeanItem(idbeacon, function(err){
            callback(err)
        })
    }
}
BeanItemPage.propTypes = {
    BeanItemStore: React.PropTypes.object
}

export default BeanItemPage;
