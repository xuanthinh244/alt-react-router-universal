var React = require('react');
import BeanListStore from '../../stores/BeanListStore'
import BeanListActions from '../../actions/BeanListActions'
import BeanListItem from '../bean/BeanListItem'

class BeanListPage extends React.Component {

    render() {
        var beanList = null;
        var currentState = this.props.BeanListStore;

        if (currentState.loadingBeanList) {
            beanList = (
                <div className="loading-element">LOADING...</div>
            );
        } else {
            beanList = currentState.beanList.map(function(listItem) {
                return (
                    <BeanListItem key={listItem.bean_id} bean={listItem} />
                );
            })
        }

        return (
            <div>
                <h2>Take your pick </h2>
                <ul>
                    { beanList }
                </ul>
            </div>
        );
    }
    static fetchData(routerState, callback) {
        BeanListActions.requestBeanList(callback);
    }
}

BeanListPage.propTypes = {
    BeanListStore: React.PropTypes.object
};



export default BeanListPage;
