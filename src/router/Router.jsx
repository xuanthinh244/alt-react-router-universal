'use strict';

/*=================================
 Libraries
 =================================*/
import _ from 'lodash';
import alt from '../alt';
import async from 'async';
import Iso from 'iso';

/*=================================
 Configs
 =================================*/
import appConfig from '../configs/app';

/*=================================
 React & Router
 =================================*/
import React from 'react';
import { match, RoutingContext, Router } from 'react-router'

import { renderToString } from 'react-dom/server'
import { render } from 'react-dom'

import createBrowserHistory from 'history/lib/createBrowserHistory';
/*=================================
 Google Analytics
 =================================*/
import GoogleAnalytics from 'react-ga';

/*=================================
 Routes
 =================================*/
import routes from './Routes.jsx';

/*=================================
 Router
 =================================*/

function getDataForRoutes (states, callback) {
    async.waterfall([
        callback => {
            // Loop through the matching routes

            let routesWithData = states.routes.filter((route) => {
                return route.component && route.component.fetchData;
            });
            let routesWithMetadata = states.routes.filter((route) => {
                return route.component && route.component.generateMetadata;
            });
            let routeWithMetadata = null;

            // We always take the last one route with meta data.
            if (routesWithMetadata.length >= 1) {
                routeWithMetadata = routesWithMetadata[routesWithMetadata.length - 1];
            }
            callback(null, routesWithData, routeWithMetadata);
        },

        (routesWithData, routeWithMetadata, callback) => {
            async.map(routesWithData, (route, fetchDataCallback) => {
                // Fetch data for each route
                route.component.fetchData(states, fetchDataCallback);
            }, error => {
                callback(error, routeWithMetadata);
            });
        },

        (routeWithMetadata, callback) => {
            let metadata = _.cloneDeep(appConfig.metadata);
            if (routeWithMetadata != null) {
                _.merge(metadata, routeWithMetadata.component.generateMetadata(states));
            }
            callback(null, metadata);
        }
    ], callback);
}

export default class AppRouter {
    /**
     * Client side router initialization.
     * @param data
     */
    static init( containerReference ) {
        render(
            <Router routes={routes} history={createBrowserHistory()} />,
            containerReference
        )
    }

    /**
     * Server side rendering - Data retrieval
     */
    static getData(req, res, next) {
        match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
            if (error) {
                res.status(500).send(error.message)
            } else if (redirectLocation) {
                res.redirect(302, redirectLocation.pathname + redirectLocation.search)
            } else if (renderProps) {
                res.local = {
                    states : renderProps
                };

                let callback = (error, appConfig) => {{}
                    if(!error){
                        res.local.appConfig = appConfig;
                        next()
                    }else{
                        next(error);
                    }
                };

                getDataForRoutes(renderProps, callback);
            } else {
                res.status(404).send('Not found')
            }

        })
    }

    /**
     * Server side rendering - Website serving
     */
    static serve(req, res, next) {
        let iso = new Iso();

        let appConfig = res.local.appConfig;
        let renderProps = res.local.states;

        let htmlBody = renderToString(<RoutingContext {...renderProps} />);
        let data = alt.flush(); // Take a snapshot of the datastores and flush it
        console.log(data);


        iso.add(htmlBody, data); // Add the data snapshot to the response

        res.render('index', {
            app: appConfig,
            body: iso.render(),
            metadata: appConfig
        });
    }
}
