'use strict';

/*=================================
 React & Router
 =================================*/
import React from 'react';
import { Router, Route, IndexRoute, Link, IndexLink } from 'react-router'

/*=================================
 Components
 =================================*/
import App from '../components/App';
import BeanListPage from '../components/page/BeanListPage'
import BeanItemPage from '../components/page/BeanItemPage'
import BeanItemEditPage from '../components/page/BeanItemEditPage'

/*=================================
 ROUTES
 =================================*/

let routes = (
    <Router>
        <Route path="/" component={App}>
            <IndexRoute name="beanList" component={BeanListPage}/>
            <Route name="beanItem" path="/:beanId" component={BeanItemPage}/>
        </Route>
    </Router>
);
export default routes;
